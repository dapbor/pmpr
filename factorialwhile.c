#include <stdio.h>
int input()
{
    int x;
    printf("Enter the value of x:");
    scanf("%d",&x);
    return x;
}

int calc (int x)
{
    int fact=x,i=1;
    while(i<x)
    {
    fact=fact*i;
    i++;
    }
    return fact;
}

void output(int x,int fact)
{
    printf("The factorial of %d is %d.",x,fact);
}

int main()
{
    int x=input();
    int fact=calc(x);
    output(x,fact);
    return 0;
} 
